#include <cstdio>
using namespace std;

int main(){
	int k, m;
	scanf("%d %d", &k, &m);
	bool yes;
	while(k != 0){
		int fc[k];
		yes = true;
		for(int i = 0; i < k; i++){
			scanf("%d", &fc[i]);
		}
		int req;
		int cc;
		for(int i = 0; i < m; i++){
			scanf("%d", &cc);
			scanf("%d", &req);
			int currentCourse;
			int reqCounter = 0;
			for(int j = 0; j < cc; j++){
				scanf("%d", &currentCourse);
				for(int x = 0; x < k; x++){
					if(fc[x] == currentCourse)
						reqCounter++;
				}
			}
			if(reqCounter < req){
				yes = false;
			}
		}
		if(yes)
			printf("yes\n");
		else
			printf("no\n");

		scanf("%d %d", &k, &m);
	}
}