#include <cstdio>
using namespace std;

int main(){
	int h, d, day;
	float f, u, fatigue, currentHeight;
	scanf("%d %f %d %f", &h, &u, &d, &f);
	while(h){
		// Calculate fatigue in distance
		fatigue = u*(f/100);
		day = 1;
		currentHeight = u;
		//printf("WellHeight: %d, fatigue: %f, Up: %f, down: %d\n", h, fatigue, u, d);
		while(currentHeight <= h){
			//printf("Current Height: %f at start of day %d adding %f\n", currentHeight, day, (u-fatigue));
			if(day == 1)
				currentHeight -=d;
			if(currentHeight < 0){
				printf("failure on day %d\n", day);
				break;
			}
			day++;
			if((u-fatigue) > 0)
				currentHeight += (u-fatigue);
			if(currentHeight >= h)
				break;
			currentHeight -= d;
			u -= fatigue;
		}
		if(currentHeight >= h)
			printf("success on day %d\n", day); // Day -1 to account for extra incriment before exiting while loop
		scanf("%d %f %d %f", &h, &u, &d, &f);
	}
}