#include <iostream>
#include <string>
using namespace std;

int main(){
	int l;
	string line;
	cin >> l;
	int dist, r, d;
	while(l){
		cin.ignore();
		getline(cin, line);
		char cur;
		dist = l+1;
		r = d = -1;
		for(int i = 0; i < l; i++){
			cur = line[i];
			if(cur == 'R'){
					r = i;
			}else if(cur == 'D'){
					d = i;
			}else if(cur == 'Z'){
				dist = 0;
				break;
			}
			if(r > -1 && d > -1)
				if(abs(r-d) < dist)
					dist = abs(r-d);
		}
		cout << dist << endl;
		cin >> l;
	}
}