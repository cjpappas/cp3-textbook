#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

int main(){
	string sequence;
	int n;
	int i = 1;
	while(cin >> sequence){
		scanf("%d\n", &n);
		int start, end;
		string sub;
		bool flag;
		cout << "Case " << i << ":" << endl;
		while(n--){
			scanf("%d %d\n", &start, &end);
			if(end < start){
				int temp = end;
				end = start;
				start = temp;
			}
			sub = sequence.substr(start, end+1);
			char zero = sub[0];
			flag = true;
			for(int i = 1; i < end-start+1; i++){
				if(sub[i] != zero){
					flag = false;
					break;
				}
			}
			if(flag)
				cout << "Yes" << endl;
			else
				cout << "No" << endl;
		}
		i++;
	}

}