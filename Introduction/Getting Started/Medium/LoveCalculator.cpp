#include <iostream>
#include <cstdio>
#include <string>
#include <map>
using namespace std;

int calcVal(int num){
	if(num < 10)
		return num;
	int sum = num%10;
	while((num = num/10)){
		sum += num%10;
	}
	return calcVal(sum);

}

int main(){
	map<char, int> vals;
	vals['a'] = 1; vals['A'] = 1;
	vals['b'] = 2; vals['B'] = 2;
	vals['c'] = 3; vals['C'] = 3;
	vals['d'] = 4; vals['D'] = 4;
	vals['e'] = 5; vals['E'] = 5;
	vals['f'] = 6; vals['F'] = 6;
	vals['g'] = 7; vals['G'] = 7;
	vals['h'] = 8; vals['H'] = 8;
	vals['i'] = 9; vals['I'] = 9;
	vals['j'] = 10; vals['J'] = 10;
	vals['k'] = 11; vals['K'] = 11;
	vals['l'] = 12; vals['L'] = 12;
	vals['m'] = 13; vals['M'] = 13;
	vals['n'] = 14; vals['N'] = 14;
	vals['o'] = 15; vals['O'] = 15;
	vals['p'] = 16; vals['P'] = 16;
	vals['q'] = 17; vals['Q'] = 17;
	vals['r'] = 18; vals['R'] = 18;
	vals['s'] = 19; vals['S'] = 19;
	vals['t'] = 20; vals['T'] = 20;
	vals['u'] = 21; vals['U'] = 21;
	vals['v'] = 22; vals['V'] = 22;
	vals['w'] = 23; vals['W'] = 23;
	vals['x'] = 24; vals['X'] = 24;
	vals['y'] = 25; vals['Y'] = 25;
	vals['z'] = 26; vals['Z'] = 26;

	string n1, n2;
	while(getline(cin, n1)){
		getline(cin, n2);
		int n1val = 0, n2val = 0;
		float p;
		for(int i = 0; i < n1.length(); i++){
			n1val += vals[n1[i]];
		}
		for(int i = 0; i < n2.length(); i++){
			n2val += vals[n2[i]];
		}
		if(calcVal(n1val) > calcVal(n2val))
			p = (float)calcVal(n2val)/calcVal(n1val);
		else
			p = (float)calcVal(n1val)/calcVal(n2val);
		printf("%0.2f %%\n", p*100);
	}
}