#include <cstdio>
using namespace std;

int main(){
	int a, c, laser;
	scanf("%d %d\n", &a, &c);
	while(a && c){
		int cur, last;
		scanf("%d", &cur);
		laser = a-cur;
		last = cur;
		c -= 1; 
		while(c--){
			scanf("%d", &cur);
			if(cur < last){
				laser += last - cur;
			}
			last = cur;
		}
		printf("%d\n", laser);
		scanf("%d %d\n", &a, &c);
	}
}