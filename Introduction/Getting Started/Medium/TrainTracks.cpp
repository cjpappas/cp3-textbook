#include <iostream>
#include <string>
using namespace std;

int main(){
	int n;
	cin >> n;
	char start;
	string current;
	int pos;
	char cur, last;
	int mm, ff, mf;
	cin.ignore();
	while(n--){
		pos = 1;
		mm = ff = mf = 0;
		getline(cin, current);
		cur = current[pos];
		last = current[pos-1];
		while(pos < current.length()){
			if(last == 'M' && cur == 'M')
				mm++;
			else if(last == 'F' && cur == 'F')
				ff++;
			else
				mf++;
			pos += 3;
			cur = current[pos];
			last = current[pos-1];
		}
		//cout << mm << ff << endl;
		if(mm == ff && (mf+mm+ff) > 1)
			cout << "LOOP" << endl;
		else
			cout << "NO LOOP" << endl;
	}
	return 0;
}