#include <cstdio>
using namespace std; 

int main(){
	int n, m, limit;
	scanf("%d %d %d", &n, &m, &limit);
	int i = 1;
	bool blown = false;
	while(n != 0 && m != 0 && limit != 0){
		int vals[n];
		bool state[n];
		int power = 0;
		int max = 0;
		blown = false;
		for(int i = 0; i < n; i++){
			scanf("%d", &vals[i]);
			state[i] = false;
		}
		for(int i = 0; i < m; i++){
			int current;
			scanf("%d", &current);
			// Turn the appliance on
			if(!state[current-1]){
				state[current-1] = true;
				power += vals[current-1];
			} else {
				// Turn the appliance off
				state[current-1] = false;
				power -= vals[current-1];
			}
			if(power > max)
				max = power;
			if(limit < power)
				blown = true;
		}
		printf("Sequence %d\n", i);
		if(blown){
			printf("Fuse was blown.\n\n");
		} else {
			printf("Fuse was not blown.\nMaximal power consumption was %d amperes.\n\n", max);
		}
		scanf("%d %d %d", &n, &m, &limit);
		i++;
	}
}