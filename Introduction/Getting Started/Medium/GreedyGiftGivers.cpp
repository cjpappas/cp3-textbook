#include <iostream>
#include <cstdio>
#include <string>
#include <map>
#include <cmath>
#include <utility>
using namespace std;

int main(){
	int n;
	bool start = true;
	while(!cin.eof() && scanf("%d", &n) == 1){
		if(!start)
			cout << endl;
		start = false;
		map<string, int > names;
		string nameorder[n];
		for(int i = 0; i < n; i++){
			string current;
			cin >> current;
			names[current] = 0;
			nameorder[i] = current;
		}
		for(int i = 0; i < n; i++){
			string current;
			int money, presents, amount;
			cin >> current >> money >> presents;
			if(money > 0 && presents > 0)
				amount = money/presents;
			else
				amount = 0;
			names[current] -= amount*presents;
			for(int j = 0; j < presents; j++){
				string name;
				cin >> name;
				names[name] += amount;
			}
		}
		if(!nameorder[0].empty()){
			for(int i = 0; i < n; i++){
				cout << nameorder[i] << " " << names[nameorder[i]] << endl;
			}
		}
	}
}