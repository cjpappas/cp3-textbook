#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

int main() {
	int r, n;
	string x;
	int count = 1;
	while (cin >> r >> n, r || n) {
		double price = 2000000000, p;
		int reqMet = -1, rm;
		string name, nam;
		// Chew up req names, unimportant
		for (int i = 0; i < r; i++) {
			cin.ignore(); // Make sure to flush buffer for \n
			getline(cin, x);
		}
		while (n--) {
			// Current proposal
			getline(cin,nam);
			scanf("%lf %d\n", &p, &rm);
			// If it has more reqs met then we have seen so far
			if (rm > reqMet) {
				reqMet = rm;
				name = nam;
				price = p;
			} else if (rm == reqMet && p < price) {
				// Otherwise if it has the same reqs met but a lower price
				reqMet = rm;
				name = nam;
				price = p;
			}
			// Chew up reqs met, unimportant
			for (int i = 0; i < rm; i++) {
				getline(cin, x);
			}
		}
		// Add extra blank line
		if (count > 1)
			cout << endl;
		// Print answer
		cout << "RFP #" << count << endl;
		cout << name << endl;
		count++;
	}
	return 0;
}