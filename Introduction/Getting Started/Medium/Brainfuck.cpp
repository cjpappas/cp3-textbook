#include <cstdio>
#include <string>
#include <iostream>
using namespace std;

int main(){
	int t, i;
	scanf("%d\n", &t);
	string line;
	int buffer[101];
	int bp;
	i = 1;
	while(t--){
		getline(cin, line);
		bp = 0;
		for(int j = 0; j < 100; j++){
			buffer[j] = 0;
		}
		for(int j = 0; j < line.length(); j++){
			if(line[j] == '>')
				bp = (bp+1)%100;
			else if(line[j] == '<'){
				bp = (bp-1);
				if(bp < 0)
					bp = 100+bp;
			} else if(line[j] == '+')
				buffer[bp] = (buffer[bp]+1)%256;
			else if(line[j] == '-'){
				buffer[bp] = (buffer[bp]-1);
				if(buffer[bp] < 0)
					buffer[bp] = 256+buffer[bp];
			}
		}
		printf("Case %d: ", i);
		for(int j = 0; j < 100; j++){
			if(j == 99)
				printf("%02X\n", buffer[j]);
			else
				printf("%02X ", buffer[j]);
		}
		i++;
	}
}