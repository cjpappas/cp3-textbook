#include <cstdio>
#include <math.h>
using namespace std;

int main(){
	int TC;
	float n, m;
	scanf("%d", &TC);
	while(TC--){
		scanf("%f %f", &n, &m);
		printf("%d\n", (int)(ceil((n-2)/3) * ceil((m-2)/3)));
	}
}