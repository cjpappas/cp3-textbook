#include <cstdio>
using namespace std;

int main(){
	int TC, a, b, c, i;
	scanf("%d", &TC);
	i = 1;
	while(TC--){
		scanf("%d %d %d", &a, &b, &c);
		if(a > 20 || b > 20 || c > 20){
			printf("Case %d: bad\n", i);
		} else
			printf("Case %d: good\n", i);
		i++;
	}
}