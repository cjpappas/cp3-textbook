#include <cstdio>
using namespace std;

int main(){
	int n, temp, total;
	int i = 1;
	while(scanf("%d", &n), n){
		total = 0;
		for(int i = 0; i < n; i++){
			scanf("%d", &temp);
			if(temp > 0)
				total ++;
			else
				total --;
		}
		printf("Case %d: %d\n", i, total);
		i++;
	}
}