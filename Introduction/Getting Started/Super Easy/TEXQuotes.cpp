#include <iostream>
#include <string>
using namespace std;

int main(){
	char c;
	string final = "";
	string q1 = "``";
	string q2 = "''";
	bool quote = true;
	while((c = getchar()) != EOF){
		if(c == '"'){
			if(quote){
				final.append(q1);
				quote = !quote;
			} else {
				final.append(q2);
				quote = !quote;
			}
		} else {
			final += c;
		}
	}
	cout << final;
}