#include <cstdio>
#include <string>
#include <iostream>
using namespace std;

int main(){
	int TC;
	string line;
	char t = 't';
	char w = 'w';
	char o = 'o';
	scanf("%d", &TC);
	while(TC--){
		cin >> line;
		if(line.length() == 5)
			printf("3\n");
		else if((line.at(0) == t && line.at(1) == w) || (line.at(0) == t && line.at(2) == o) ||
			(line.at(1) == w && line.at(2) == o))
			printf("2\n");
		else
			printf("1\n");
	}
}