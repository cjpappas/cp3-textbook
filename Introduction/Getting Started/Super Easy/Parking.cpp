#include <cstdio>
using namespace std;

int main(){
	int TC, n, min, max, temp;
	scanf("%d", &TC);
	while(TC--){
		scanf("%d", &n);
		min = 99;
		max = 0;
		for(int i = 0; i < n; i++){
			scanf("%d", &temp);
			if(temp < min)
				min = temp;
			if(temp > max)
				max = temp;
		}
		printf("%d\n", 2*(max-min));
	}
}