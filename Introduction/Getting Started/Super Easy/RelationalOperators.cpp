#include <cstdio>
using namespace std;

int main(){
	int a, b, TC;
	scanf("%d", &TC);
	while(TC--){
		scanf("%d %d", &a, &b);
		if(a < b)
			printf("<\n");
		else if (a > b)
			printf(">\n");
		else
			printf("=\n");
	}
}