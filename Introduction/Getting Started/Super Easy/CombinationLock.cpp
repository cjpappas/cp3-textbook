#include <cstdio>
using namespace std;

int degrees(int a, int b, int dir){
	if(!dir)
		if(a <= b)
			return b-a;
		else
			return 40-(a-b);
	else
		if(a >= b)
			return a-b;
		else
			return 40-(b-a);
}

int main(){
	int s, a, b, c;
	int total = 0;
	while(scanf("%d %d %d %d", &s, &a, &b, &c), (s || a || b || c)){
		total += 1080;
		total += degrees(s, a, 1)*9;
		total += degrees(a, b, 0)*9;
		total += degrees(b, c, 1)*9;
		printf("%d\n", total);
		total = 0;
	}
}