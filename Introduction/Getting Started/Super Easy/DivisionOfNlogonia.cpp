#include <cstdio>
#include <utility>
using namespace std;

int main(){
	int k;
	while(scanf("%d", &k), k){
		pair<int, int> division;
		scanf("%d %d", &division.first, &division.second);
		int a, b;
		while(k--){
			scanf("%d %d", &a, &b);
			if(a == division.first || b == division.second)
				printf("divisa\n");
			else if(a < division.first)
				if(b > division.second)
					printf("NO\n");
				else
					printf("SO\n");
			else
				if(b > division.second)
					printf("NE\n");
				else
					printf("SE\n");

		}
	}
}