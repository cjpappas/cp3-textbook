#include <cstdio>
#include <cstring>
using namespace std;

int main(){
	int TC, m, res;
	res = 0;
	char str[8];
	scanf("%d", &TC);
	while(TC--){
		if(scanf("%8s %d", str, &m) == 2){
			res += m;
		} else{
			printf("%d\n", res);
		}
	}
}