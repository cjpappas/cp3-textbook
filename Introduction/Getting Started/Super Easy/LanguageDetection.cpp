#include <cstdio>
#include <string>
#include <iostream>
using namespace std;

int main(){
	string line;
	int i = 1;
	while(cin >> line){
		if(line.find("#") != string::npos)
			break;
		else
			if(line.find("HELLO") != string::npos && line.length() == 5)
				printf("Case %d: ENGLISH\n", i);
			else if(line.find("HOLA") != string::npos && line.length() == 4)
				printf("Case %d: SPANISH\n", i);
			else if(line.find("HALLO") != string::npos && line.length() == 5)
				printf("Case %d: GERMAN\n", i);
			else if(line.find("BONJOUR") != string::npos && line.length() == 7)
				printf("Case %d: FRENCH\n", i);
			else if(line.find("CIAO") != string::npos && line.length() == 4)
				printf("Case %d: ITALIAN\n", i);
			else if(line.find("ZDRAVSTVUJTE") != string::npos && line.length() == 12)
				printf("Case %d: RUSSIAN\n", i);
			else
				printf("Case %d: UNKNOWN\n", i);
		i++;
	}
}