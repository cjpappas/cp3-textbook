#include <string>
#include <iostream>
using namespace std;

int main(){
	int n, counter;
	cin >> n;
	string names[n];
	string song[] = {"Happy", "birthday", "to", "you", "Happy", "birthday", "to", "you", 
						"Happy", "birthday", "to", "Rujia", "Happy", "birthday", "to", "you"};
	for(int i = 0; i < n; i++){
		cin >> names[i];
	}
	counter = 0;
	while(1){
		if(counter >= n && (counter % 16) == 0)
			break;
		cout << names[counter%n] << ": " << song[counter%16] << endl;
		counter ++;
	}

}

/*
Mom: Happy
Dad: birthday
Girlfriend: to
Mom: you
Dad: Happy
Girlfriend: birthday
Mom: to
Dad: you
Girlfriend: Happy
Mom: birthday
Dad: to
Girlfriend: Rujia
Mom: Happy
Dad: birthday
Girlfriend: to
Mom: you
*/