#include <cstdio>
#include <cstdlib>
using namespace std;

int main(){
	int TC, W, a, b, diff;
	scanf("%d", &TC);
	while(TC--){
		scanf("%d", &W);
		scanf("%d %d", &a, &b);
		diff = a-b;
		bool possible = true;
		for(int i = 0; i < W-1; i++){
			scanf("%d %d", &a, &b);
			if((a-b) != diff)
				possible = false;
		}
		if(possible)
			printf("yes\n");
		else
			printf("no\n");

		if(TC)
			printf("\n");
	}
}