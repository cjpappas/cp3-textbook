#include <cstdio>
using namespace std;

int main(){
	int TC, N, curr, next, i, high, low;
	scanf("%d", &TC);
	i = 1;
	while(TC--){
		scanf("%d", &N);
		scanf("%d", &curr);
		high = low = 0;
		for(int i = 1; i < N; i++){
			scanf("%d", &next);
			if(next > curr)
				high++;
			else if(next < curr)
				low++;
			curr = next;
		}
		printf("Case %d: %d %d\n", i, high, low);
		i++;
	}
}