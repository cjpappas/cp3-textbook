#include <cstdio>
#include <cstring>
using namespace std;

int main(){
	int TC, n, p;
	scanf("%d", &TC);
	char instructions[100][20];
	char temp[20];
	// Loop over test cases
	while(TC--){
		scanf("%d", &n);
		// Reset p
		p = 0;
		// Store instructions and compute p as you go
		for(int i = 0; i < n; i++){
			// Scan instructions in
			scanf("%s", instructions[i]);
			if(strcmp(instructions[i], "RIGHT") != 0 && strcmp(instructions[i], "LEFT") != 0){
				int temp;
				scanf("%*s %d", &temp);
				strcpy(instructions[i], instructions[temp-1]);
			}
		}
		// Read off instructions
		for(int i = 0; i < n; i++){
			if(!strcmp(instructions[i], "LEFT"))
				p--;
			else if(!strcmp(instructions[i], "RIGHT"))
				p++;
		}
		printf("%d\n", p);
	}
}