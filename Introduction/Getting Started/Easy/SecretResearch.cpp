#include <cstdio>
#include <iostream>
#include <string>
using namespace std;

int main(){
	int TC;
	string buff;
	scanf("%d", &TC);
	while(TC--){
		cin >> buff;
		if(((buff[0] == '1' || buff[0] == '4')&& buff.length() < 2) || (buff[0] == '7' && buff[1] == '8' && buff.length() < 3))
			printf("+\n");
		else if(buff[buff.length()-1] == '5' && buff[buff.length()-2] == '3')
			printf("-\n");
		else if(buff[0] == '9' && buff[buff.length()-1] == '4')
			printf("*\n");
		else
			printf("?\n");
	}
}