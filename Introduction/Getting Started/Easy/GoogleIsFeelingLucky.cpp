#include <cstdio>
#include <utility>
using namespace std;

int main(){
	int TC, i, max, temp;
	pair<char[100], int> sites[10];
	scanf("%d", &TC);
	i = 1;
	while(TC--){
		max = 0;
		for(int i = 0; i < 10; i++){
			scanf("%s %d", sites[i].first, &sites[i].second);
			//printf("%s %d\n", sites[i].first, sites[i].second);
			if(sites[i].second > max)
				max = sites[i].second;
		}
		printf("Case #%d:\n", i);
		for(int i = 0; i < 10; i++){
			//printf("%s %d\n", sites[i].first, sites[i].second);
			if(sites[i].second == max)
				printf("%s\n", sites[i].first);
		}
		i++;
	}
}