#include <cstdio>
using namespace std;

int main(){
	int TC, n, current, max, i;
	scanf("%d", &TC);
	i = 1; // Keep track of case number
	while(TC--){
		scanf("%d", &n);
		while(n--){
			scanf("%d", &current);
			if(current > max)
				max = current;
		}
		printf("Case %d: %d\n", i++, max);
		max = 0;
	}
}