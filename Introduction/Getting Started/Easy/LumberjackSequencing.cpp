#include <cstdio>
using namespace std;

int main(){
	int TC, start, current;
	bool inc, order;
	scanf("%d", &TC);
	printf("Lumberjacks:\n");
	while(TC--){
		scanf("%d", &start);
		scanf("%d", &current);
		if(start < current)
			inc = true;
		else
			inc = false;
		order = true;
		for(int i = 0; i < 8; i++){
			start = current;
			scanf("%d", &current);
			if(inc && start > current && order){
				order = false;
			} else if (!inc && start < current && order){
				order = false;
			}
		}
		if(order)
			printf("Ordered\n");
		else
			printf("Unordered\n");
	}
}