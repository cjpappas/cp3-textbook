#include <cstdio>
#include <cmath>
using namespace std;

int main(){
	int T, N, i;
	float m, j, temp;
	scanf("%d", &T);
	i = 1;
	while(T--){
		scanf("%d", &N);
		m = 0;
		j = 0;
		for(int i = 0; i < N; i++){
			scanf("%f", &temp);
			m += ceil((temp+1)/30);
			j += ceil((temp+1)/60);
		}
		if((int)m*10 < (int)j*15)
			printf("Case %d: Mile %d\n", i, (int)m*10);
		else if((int)m*10 == (int)j*15)
			printf("Case %d: Mile Juice %d\n", i, (int)m*10);
		else
			printf("Case %d: Juice %d\n", i, (int)j*15);
		i++;
	}
}