#include <cstdio>
using namespace std;

int main(){
	int n, b, h, w, price, beds, min, weeks;
	min = 500001;
	while(scanf("%d %d %d %d", &n, &b, &h, &w) > 0){
		min = 500001;
		//printf("People staying: %d Budget: %d Hotels: %d Weeks: %d\n", n, b, h, w);
		while(h--){
			// Current hotel price
			scanf("%d", &price);
			//printf("Hotel #%d: %d\n", h, price);
			weeks = w;
			while(weeks--){
				// Available bed for each weekend
				scanf("%d", &beds);
				//printf("Bed free in week #%d: %d\n", w, beds);
				if(n <= beds && (price*n) <= b)
					if((price*n) < min)
						min = price*n;
			}
		}
		if(min < 500001)
			printf("%d\n", min);
		else
			printf("stay home\n");
		}
}