#include <cstdio>
#include <cmath>
using namespace std;

int main(){
	int TC, f;
	long long prem;
	double size, animals, env;
	scanf("%d", &TC);
	while(TC--){
		scanf("%d", &f);
		prem = 0;
		while(f--){
			scanf("%lf %lf %lf", &size, &animals, &env);
			prem += (long long)(size*env);
		}
		printf("%lld\n", prem);
	}
}