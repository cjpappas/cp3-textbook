#include <cstdio>
using namespace std;

int main(){
	int b, n, from, to, amt;
	scanf("%d %d", &b, &n);
	bool yes;
	while(b && n){
		yes = true;
		int banks[b];
		for(int i = 0; i < b; i++)
			scanf("%d", &banks[i]);
		for(int i = 0; i < n; i++){
			scanf("%d %d %d", &from, &to, &amt);
			banks[from-1] -= amt;
			banks[to-1] += amt;
		}
		for(int i = 0; i < b; i++){
			if(banks[i] < 0)
				yes = false;
		}
		if(yes)
			printf("S\n");
		else
			printf("N\n");

		scanf("%d %d", &b, &n);
	}
}