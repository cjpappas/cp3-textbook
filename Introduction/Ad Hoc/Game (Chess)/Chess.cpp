#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

int main(){
	int t, w, h, ans;
	char piece;
	scanf("%d\n", &t);
	while(t--){
		scanf("%c %d %d\n", &piece, &w, &h);
		switch(piece){
			case 'r':
			ans = min(w, h);
			break;
			case 'k':
			ans = ceil((float)(w*h)/2);
			break;
			case 'Q':
			ans = min(w, h);
			break;
			case 'K':
			ans = ceil((float)w/2)*ceil((float)h/2);
			break;
		}
		printf("%d\n", ans);
	}
}