#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

/*
Numbering for int array
    10      12
9   1   2   3   11
-   4   X   5   -
13  6   7   8   15
    14      16   
*/

void removeSquares(int squares[], int size, int pos, bool freeSpace[9][9], bool row, char board[9][9]){
	for(int i = 0; i < size; i++){
		switch(squares[i]){
		case 1:
		if(pos > 7 && (pos%8 != 0)){
			if(row)
				for(int x = 1; ((pos/8)-x >= 0 && (pos%8)-x >= 0); x++){
					if(board[(pos/8)-x][(pos%8)-x] == 'F')
						freeSpace[(pos/8)-x][(pos%8)-x] = false;
					else
						break;
				}
			else
				freeSpace[(pos/8)-1][(pos%8)-1] = false;
		}
		break;
		case 2:
		if(pos > 7){
			if(row)
				for(int x = 1; (pos/8)-x >= 0; x++)
					if(board[(pos/8)-x][pos%8] == 'F')
						freeSpace[(pos/8)-x][pos%8] = false;
					else
						break;
			else
				freeSpace[(pos/8)-1][pos%8] = false;
		}
		break;
		case 3:
		if(pos > 7 && (pos%8 != 7)){
			if(row)
				for(int x = 1; ((pos/8)-x >= 0 && (pos%8)+x < 8); x++)
					if(board[(pos/8)-x][(pos%8)+x] == 'F')
						freeSpace[(pos/8)-x][(pos%8)+x] = false;
					else
						break;
			else
				freeSpace[(pos/8)-1][(pos%8)+1] = false;
		}
		break;
		case 4:
		if(pos%8 != 0){
			if(row)
				for(int x = 1; (pos%8)-x >= 0; x++)
					if(board[pos/8][(pos%8)-x] == 'F')
						freeSpace[pos/8][(pos%8)-x] = false;
					else
						break;
			else
				freeSpace[pos/8][(pos%8)-1] = false;
		}
		break;
		case 5:
		if(pos%8 != 7){
			if(row)
				for(int x = 1; (pos%8)+x < 8; x++)
					if(board[pos/8][(pos%8)+x] == 'F')
						freeSpace[pos/8][(pos%8)+x] = false;
					else
						break;
			else
				freeSpace[pos/8][(pos%8)+1] = false;
		}
		break;
		case 6:
		if(pos < 56 && (pos%8 != 0)){
			if(row)
				for(int x = 1; ((pos/8)+x < 8 && (pos%8)-x >= 0); x++)
					if(board[(pos/8)+x][(pos%8)-x] == 'F')
						freeSpace[(pos/8)+x][(pos%8)-x] = false;
					else
						break;
			else
				freeSpace[(pos/8)+1][(pos%8)-1] = false;
		}
		break;
		case 7:
		if(pos < 56){
			if(row)
				for(int x = 1; (pos/8)+x < 8; x++)
					if(board[(pos/8)+x][pos%8] == 'F')
						freeSpace[(pos/8)+x][pos%8] = false;
					else
						break;
			else
				freeSpace[(pos/8)+1][pos%8] = false;
		}
		break;
		case 8:
		if(pos < 56 && (pos%8 != 7)){
			if(row)
				for(int x = 1; ((pos/8)+x < 8 && (pos%8)+x < 8); x++)
					if(board[(pos/8)+x][(pos%8)+x] == 'F')
						freeSpace[(pos/8)+x][(pos%8)+x] = false;
					else
						break;
			else
				freeSpace[(pos/8)+1][(pos%8)+1] = false;
		}
		break;
		case 9:
		if(pos > 7 && (pos%8 > 1)){
			freeSpace[(pos/8)-1][(pos%8)-2] = false;
		}
		break;
		case 10:
		if(pos > 15 && (pos%8 > 0)){
			freeSpace[(pos/8)-2][(pos%8)-1] = false;
		}
		break;
		case 11:
		if(pos > 7 && (pos%8 < 6)){
			freeSpace[(pos/8)-1][(pos%8)+2] = false;
		}
		break;
		case 12:
		if(pos > 15 && (pos%8 < 7)){
			freeSpace[(pos/8)-2][(pos%8)+1] = false;
		}
		break;
		case 13:
		if(pos < 56 && (pos%8 > 1)){
			freeSpace[(pos/8)+1][(pos%8)-2] = false;
		}
		break;
		case 14:
		if(pos < 48 && (pos%8 > 0)){
			freeSpace[(pos/8)+2][(pos%8)-1] = false;
		}
		break;
		case 15:
		if(pos < 56 && (pos%8 < 6)){
			freeSpace[(pos/8)+1][(pos%8)+2] = false;
		}
		break;
		case 16:
		if(pos < 48 && (pos%8 < 7)){
			freeSpace[(pos/8)+2][(pos%8)+1] = false;
		}
		break;
		}
	}
	return;
}

int main(){
	string line;
	char board[9][9];
	bool freeSpace[9][9];
	while(getline(cin, line)){
		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++){
				board[i][j] = 'F';
				freeSpace[i][j] = true;
		}
		for(int i = 0, pos = 0; i < line.length(); i++){
			if(line[i] != '/') {
				if(atoi(&line[i]) <= 8 && atoi(&line[i]) > 0){
					pos += atoi(&line[i]);
				} else {
					board[pos/8][pos%8] = line[i];
					freeSpace[pos/8][pos%8] = false;
					pos++;
				}
			}
		}
		for(int pos = 0; pos < 64; pos++){
			switch(board[pos/8][pos%8]){
				case 'k':
				case 'K':
				{
					int squares[8] = {1, 2, 3, 4, 5, 6, 7, 8};
					removeSquares(squares, 8, pos, freeSpace, false, board);
					break;
				}
				case 'q':
				case 'Q':
				{
					int squares[8] = {1, 2, 3, 4, 5, 6, 7, 8};
					removeSquares(squares, 8, pos, freeSpace, true, board);
					break;
				}
				case 'b':
				case 'B':
				{
					int squares[4] = {1, 3, 6, 8};
					removeSquares(squares, 4, pos, freeSpace, true, board);
					break;
				}
				case 'r':
				case 'R':
				{
					int squares[4] = {2, 4, 5, 7};
					removeSquares(squares, 4, pos, freeSpace, true, board);
					break;
				}
				case 'p':
				{
					int squares[2] = {6, 8};
					removeSquares(squares, 2, pos, freeSpace, false, board);
					break;
				}
				case 'P':
				{
					int squares[2] = {1, 3};
					removeSquares(squares, 2, pos, freeSpace, false, board);
					break;
				}
				case 'n': // need to implement knights
				case 'N':
				{
					int squares[8] = {9, 10, 11, 12, 13, 14, 15, 16};
					removeSquares(squares, 8, pos, freeSpace, false, board);
					break;
				}
			}
		}
		/*for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++)
				cout << board[i][j] << " ";
			cout << endl;
		}
		cout << endl;*/
		int result = 0;
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				//cout << freeSpace[i][j] << " ";
				if(freeSpace[i][j] == 1)
					result++;
			}
			//cout << endl;
		}
		cout << result << endl;
	}
}