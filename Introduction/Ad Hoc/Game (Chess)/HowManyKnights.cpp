#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

int main(){
	int w, h, res, minN, maxN;
	while(scanf("%d %d\n", &w, &h) && (w||h)){
		minN = min(w, h);
		maxN = max(w, h);
		if(minN == 1)
			res = maxN;
		else if (minN == 2)
			res = 4*int(maxN/4)+2*min(2, maxN%4);
		else
			res = (int)ceil((float)(w*h)/2);
		printf("%d knights may be placed on a %d row %d column board.\n", res, w, h);
	}
}