#include <cstdio>
#include <algorithm>
using namespace std;

int main(){
	int t, x, y, i = 1;
	int pointer, removedCards;
	scanf("%d\n", &t);
	char cards[52][3];
	while(t--){
		y = 0;
		// Scan in all cards bottom -> top
		for(int i = 0; i < 52; i++)
			scanf("%s", cards[i]);
		// To represent taking top 25 cards off top
		pointer = 24;
		removedCards = 0;
		for(int i = 0; i < 3; i++){
			if(atoi(&cards[pointer][0]) > 1 && atoi(&cards[pointer][0]) < 10){
				x = atoi(&cards[pointer][0]);
				y += x;
				pointer -= 10-x+1;
				removedCards += 10-x+1;
			} else {
				y += 10;
				pointer -= 1;
				removedCards ++;
			}
		}
		printf("Case %d: %s\n", i, cards[y+removedCards-1]);
		i++;
	}
}