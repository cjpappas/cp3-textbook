#include <cstdio>
#include <algorithm>
using namespace std;

int nextCard(int current, int a, int b, int c, int x, int y){
	int i = current+1;
	while(i == a || i == b || i == c || i == x || i == y){
		i++;
	}
	if(i < 53)
		return i;
	else
		return -1;
}

int main(){
	int a, b, c, x, y, smin, smid, smax, bmin, bmax;
	while(scanf("%d %d %d %d %d\n", &a, &b, &c, &x, &y) && (a||b||c||x||y)){
		// Get sisters lowest card, he has to atleast beat one of her cards
		smin = min(a, b);
		if(smin == a) {
			smin = min(a, c);
			if(smin == a){
				smax = max(b, c);
				if(smax == b)
					smid = c;
				else
					smid = b;
			} else {
				smax = max(a, b);
				if(smax == a)
					smid = b;
				else
					smid = a;
			}
		} else {
			smin = min(b, c);
			if(smin == b){
				smax = max(a, c);
				if(smax == a)
					smid = c;
				else
					smid = a;
			} else {
				smax = max(a, b);
				if(smax == a)
					smid = b;
				else
					smid = a;
			}
		}
		bmin = min(x, y);
		bmax = max(x, y);
		//printf("%d %d %d\n", smin, smid, smax);
		if(bmin > smax)
			printf("%d\n", nextCard(0, a, b, c, x, y));
		else if(bmin > smid)
			printf("%d\n", nextCard(smid, a, b, c, x, y));
		else if(bmax > smax)
			printf("%d\n", nextCard(smax, a, b, c, x, y));
		else
			printf("-1\n");

	}
}