#include <cstdio>
#include <map>
using namespace std;

int main(){
	char cards[13][3];
	int score, subflag, max;
	bool allStop;
	char maxSuit;
	map<char, int> counts;
	map<char, bool> stopped;
	while(scanf("%s", cards[0]) != EOF){
		score = 0;
		counts['S'] = 0; counts['H'] = 0; counts['D'] = 0; counts['C'] = 0;
		stopped['S'] = false; stopped['H'] = false; stopped['D'] = false; stopped['C'] = false;
		subflag = 0;
		allStop = true;
		for(int i = 1; i < 13; i++)
			scanf("%s", cards[i]);
		for(int i = 0; i < 13; i++){
			switch(cards[i][0]){
				case 'A':
				score += 4;
				stopped[cards[i][1]] = true;
				break;
				case 'K':
				score += 3;
				break;
				case 'Q':
				score += 2;
				break;
				case 'J':
				score += 1;
				break;
			}
			counts[cards[i][1]]++;
		}
		// Subtraction
		for(int i = 0; i < 13; i++){
			if(cards[i][0] == 'K'){
				if(counts[cards[i][1]] <= 1){
					score--;
				} else if(counts[cards[i][1]] >= 2) {
					stopped[cards[i][1]] = true;
				}
			} else if(cards[i][0] == 'Q'){
				if(counts[cards[i][1]] <= 2){
					score--;
				} else if(counts[cards[i][1]] >= 3) {
					stopped[cards[i][1]] = true;
				}
			} else if(cards[i][0] == 'J' && counts[cards[i][1]] <= 3){
				score--;
			}
		}
		// Addition
		max = 0;
		for(map<char, int>::iterator it = counts.begin(); it != counts.end(); ++it){
			if(it->second == 2){
				score++;
				subflag += 1;
			} else if(it->second < 2) {
				score += 2;
				subflag += 2;
			}
			if(it->second >= max){
				max = it->second;
				maxSuit = it->first;
			}
		}
		for(map<char, bool>::iterator it = stopped.begin(); it != stopped.end(); ++it){
			if(it->second == false)
				allStop = false;
		}
		if(score < 14)
			printf("PASS\n");
		else if((score-subflag) >= 16 && allStop)
			printf("BID NO-TRUMP\n");
		else
			printf("BID %c\n", maxSuit);
	}

}