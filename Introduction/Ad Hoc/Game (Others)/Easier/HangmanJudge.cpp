#include <iostream>
#include <string>
#include <set>
using namespace std;

int main(){
	int round, err, correct;
	string answer, guesses;
	bool found, lose, win;
	set<char> duplicates;
	while(cin >> round && round != -1){
		cin.ignore();
		getline(cin, answer);
		getline(cin, guesses);
		lose = win = false;
		err = correct = 0;
		for(int i = 0; i < guesses.length(); i++){
			found = false;
			if(duplicates.find(guesses[i]) == duplicates.end()){
				duplicates.insert(guesses[i]);
				for(int j = 0; j < answer.length(); j++){
					if(answer[j] == guesses[i]){
						found = true;
						correct++;
					}
				}
				if(!found){
					err++;
					if(err == 7){
						lose = true;
						break;
					}
				}
				if(correct == answer.length()){
					win = true;
					break;
				}
			}
		}
		cout << "Round " << round << endl;
		if(lose)
			cout << "You lose." << endl;
		else if(win)
			cout << "You win." << endl;
		else
			cout << "You chickened out." << endl;
		duplicates.clear();
	}
}