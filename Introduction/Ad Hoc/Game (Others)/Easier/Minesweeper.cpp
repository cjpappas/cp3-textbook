#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <string>
using namespace std;

char mines(int i, int j, int n, int m, string input[]){
	int counter = 0;
	if(i < n) //Check row below
		if(input[i+1][j] == '*')
			counter++;
	if(i > 0) //Check row above
		if(input[i-1][j] == '*')
			counter++;
	if(j < m) //Check right column
		if(input[i][j+1] == '*')
			counter++;
	if(j > 0) //Check left column
		if(input[i][j-1] == '*')
			counter++;
	if(i < n && j < m) // Check bottom right
		if(input[i+1][j+1] == '*')
			counter++;
	if(i < n && j > 0) //Check bottom left
		if(input[i+1][j-1] == '*')
			counter++;
	if(i > 0 && j < m) //Check top right
		if(input[i-1][j+1] == '*')
			counter++;
	if(i > 0 && j > 0)
		if(input[i-1][j-1] == '*')
			counter++;
	return counter;
}

int main(){
	int n, m, field = 1;
	while((cin >> n >> m) && (n||m)){
		cin.ignore();
		if(field != 1)
			cout << endl;
		string input[n+1];
		string final[n+1][m+1];
		for(int i = 0; i < n; i++){
			getline(cin, input[i]);
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				if(input[i][j] == '*')
					final[i][j] = '*';
				else
					final[i][j] = to_string(mines(i, j, n, m, input));
			}
		}

		cout << "Field #" << field << ":" << endl;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++)
				cout << final[i][j];
			cout << endl;
		}
		field++;
	}
}