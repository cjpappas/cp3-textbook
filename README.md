# Competitive Programming 3 Solutions
This repository contains solutions to the problems supplied by the competitve programming 3 textbook. If you would like to submit your own solutions please send an email to cameron.pappas@students.mq.edu.au. This repository was created for MQ students by both current and past MQ students.

## Rules For Submissions
* Please don't submit solutions for a problem that has already solved unless you solution is in a different language to the other solution/s.
* Please include your name as a comment at the top of your solution file so we can correctly credit you!
* Make sure your .py/cpp/java file is in the correct solution folder for the problem.
* Please make sure you file is named approipriatley: ASampleSolution.xx

## To do
* Add wiki. Pages includes (submission page with example file, contributions page, etc.)
* Clean up submissions
* Make folder for competitve libraries
* Possible graphs section to track commits etc.